#!/usr/bin/env python

import sys
import json
import subprocess

if __name__ == "__main__":
    assert len(sys.argv) >= 3
    with open(sys.argv[1], "r") as f:
        env = json.load(f)
    subprocess.run(sys.argv[2:], env=env)
