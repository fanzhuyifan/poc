# Prerequisites
- python-cairo installed on the system
- pipenv installed on the system
- setup (empty) pipenv:
```bash
pipenv install
```
# Steps
Check that cmake configures *succeeds* outside of the pipenv -- no warnings or errors:
```bash
rm -rf build ; mkdir build ; cd build ; cmake .. ; cd ..
```

Check that cmake configure *fails* inside the pipenv:
```bash
rm -rf build ; mkdir build; pipenv run bash -c "cd build ; cmake .."
```

Save environment variables to a file:
```bash
python save_env.py > env.json
```

Run cmake with the environment variables from the file -- no warnings or errors:
```bash
rm -rf build ; mkdir build ; pipenv run bash -c "cd build ; python ../run_env.py ../env.json cmake .."
```
